import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { NativeStorage } from '@ionic-native/native-storage';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  date1 = new Date("Sep 23, 2013 08:30:00");
  date2 = new Date("Sep 23, 2013 13:00:00");
  date3 = new Date("Sep 23, 2013 13:45:00");
  date4 = new Date("Sep 23, 2013 17:15:00");
  dia: string = "";
  arrayHoras = new Array;
  horas:number = 0;
  minutos:number = 0;
  diaSemana:number;
  introManual:boolean = false;
  horaManual:string = "";

  constructor(public navCtrl: NavController, private alertCtrl : AlertController, private platform: Platform, private nativeStorage: NativeStorage, private modalCtrl: ModalController) {
    if(this.platform.is("cordova")){
      this.platform.ready().then(()=>{
        this.nativeStorage.getItem("array").then((array)=>{
          console.log(array);
          if(array.length > 3){
            this.arrayHoras = array;
            this.transformarArray();
            this.calcularHorasTabla();
          }
        })
      })
    }else{
      console.log("Estamos en web");
      if(localStorage.getItem("arrayHoras")){
        console.log("Había un array guardado...")
        this.arrayHoras = JSON.parse(localStorage.getItem("arrayHoras"));
        this.transformarArray();
        this.calcularHorasTabla();
      }
    }
  }

  entradaTrabajo(){
    let dateNow;
    if(this.introManual){
       dateNow = new Date("2018-01-0" + this.diaSemana+" "+this.horaManual+":00");
    }else{
       dateNow = new Date();
    }
    this.arrayHoras[dateNow.getDay()-1][0] = dateNow;
    this.calcularHorasTabla();
    if(this.platform.is("cordova")){
      this.nativeStorage.setItem("array", this.arrayHoras)
    }

    
  }

  entradaComedor(){
    let dateNow;
    if(this.introManual){
       dateNow = new Date("2018-01-0" + this.diaSemana+" "+this.horaManual+":00");
    }else{
       dateNow = new Date();
    }
    this.arrayHoras[dateNow.getDay()-1][1] = dateNow;
    this.calcularHorasTabla();
    if(this.platform.is("cordova")){
      this.nativeStorage.setItem("array", this.arrayHoras)
    }
  }

  salidaComedor(){
    let dateNow;
    if(this.introManual){
       dateNow = new Date("2018-01-0" + this.diaSemana+" "+this.horaManual+":00");
    }else{
       dateNow = new Date();
    }
    this.arrayHoras[dateNow.getDay()-1][2] = dateNow;
    this.calcularHorasTabla();
    if(this.platform.is("cordova")){
      this.nativeStorage.setItem("array", this.arrayHoras)
    }
  }

  salidaTrabajo(){
    let dateNow;
    if(this.introManual){
       dateNow = new Date("2018-01-0" + this.diaSemana+" "+this.horaManual+":00");
    }else{
       dateNow = new Date();
    }
    this.arrayHoras[dateNow.getDay()-1][3] = dateNow;
    this.calcularHorasTabla();
    if(this.platform.is("cordova")){
      this.nativeStorage.setItem("array", this.arrayHoras)
    }
  }

  resetTabla(){
    let confirm = this.alertCtrl.create({
      title: 'Reiniciar horas?',
      message: 'Chaval, vas a reiniciar todas las horas, estás de acuerdo?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            for(let i=0; i<5; i++){
              this.arrayHoras[i] = [];
              this.arrayHoras[i][0] = this.date1;
              this.arrayHoras[i][1] = this.date2;
              this.arrayHoras[i][2] = this.date3;
              this.arrayHoras[i][3] = this.date4;
            }
            console.log(this.arrayHoras);
            this.nativeStorage.setItem("array", this.arrayHoras);
            this.calcularHorasTabla();

          }
        }
      ]
    });
    confirm.present();
  }

  calcularHorasTabla(){
    this.horas = 0;
    this.minutos = 0;
    for(let i=0; i<5; i++){
      this.minutos += (this.arrayHoras[i][3].getMinutes() - this.arrayHoras[i][2].getMinutes()) +(this.arrayHoras[i][1].getMinutes()- this.arrayHoras[i][0].getMinutes()) 
      
      this.horas += (this.arrayHoras[i][3].getHours() - this.arrayHoras[i][2].getHours()) +(this.arrayHoras[i][1].getHours()- this.arrayHoras[i][0].getHours())
    }
    while (this.minutos> 59) {
      this.minutos -= 60;
      this.horas += 1
    }

    while (this.minutos < 0) {
      this.minutos += 60;
      this.horas -= 1;
    }

    console.log("Minutos: " + this.minutos );
    console.log("Horas: " + this.horas);
    let dateNow = new Date();
    console.log(dateNow);
  }

  transformarArray(){
    for(let i=0;i<5;i++){
      for(let j=0;j<4;j++){
        this.arrayHoras[i][j] = new Date(this.arrayHoras[i][j])
      }
    }
  }

  goModal(){
    let modal = this.modalCtrl.create("modalExcel",{
      "arrayHoras": this.arrayHoras
    } );
    modal.present();
  }

  check(){
    console.log(this.introManual);
    console.log(this.diaSemana);
    console.log(this.horaManual);
    console.log(new Date("2018-01-0" + this.diaSemana+" "+this.horaManual+":00"));
  }

}
