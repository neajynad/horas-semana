import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalExcelPage } from './modal-excel';

@NgModule({
  declarations: [
    ModalExcelPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalExcelPage),
  ],
})
export class ModalExcelPageModule {}
