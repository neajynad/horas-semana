import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ModalExcelPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: "modalExcel"
})
@Component({
  selector: 'page-modal-excel',
  templateUrl: 'modal-excel.html',
})
export class ModalExcelPage {
  arrayHoras:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.arrayHoras = this.navParams.get("arrayHoras");
    console.log(this.arrayHoras)
  }

  restarHoras(hora1:Date, hora2:Date){
    let horas = hora1.getHours() - hora2.getHours();
    let minutos = hora1.getMinutes() - hora2.getMinutes();

    while(minutos>59){
      minutos -=60;
      horas += 1;
    }

    while(minutos<0){
      minutos +=60;
      horas -= 1;
    }
    return String(horas+ ":"+minutos);  
  }

  restarHorasNumber(hora1:Date, hora2:Date, hora3:Date, hora4:Date){
    let horas = (hora1.getHours() - hora2.getHours())-(hora3.getHours() - hora4.getHours())
    let minutos = (hora1.getMinutes() - hora2.getMinutes()) - ( hora3.getMinutes() - hora4.getMinutes())

    while(minutos>59){
      minutos -=60;
      horas += 1;
    }

    while(minutos<0){
      minutos +=60;
      horas -= 1;
    }
    return String(horas+ ":"+minutos);  
  }


}
